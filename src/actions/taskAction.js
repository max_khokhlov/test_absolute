import * as c from "@constants/tasks"
import * as api from "@constants/api"
import * as helpers from "@helpers";
import axios from "axios"

/**
 * Получение списка заявок
 * @returns {Function}
 */
export const getTaskList = () => async dispatch => {

    dispatch(loadingTask(true));

    try {
        const res = await axios.post(api.API_URL, {});
        dispatch(setTaskList(res.data));
    }catch (err) {
        const {status, message} = errorCatcher(err);
        helpers.errorMessage(status, message);
    }

    dispatch(loadingTask(false));

}

/**
 *
 * @param status
 * @returns {{type: string, loading: *}}
 */
export const loadingTask = status => ({
    type: c.LOAD_TASK_LIST,
    loading: status
});


/**
 * @param taskList
 * @returns {{taskList: *, type: string}}
 */
export const setTaskList = (taskList) => ({
    type: c.SET_TASK_LIST,
    taskList: taskList,
});


/**
 *
 * @param error
 * @param error.response.status
 * @param error.message
 * @returns Object
 */
const errorCatcher = error => ({
    status: error.response.status,
    message: error.message,
});
