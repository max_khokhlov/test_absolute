import React from 'react';
import TaskPage from "@components/task/pages/taskPage";

function App() {
    return <main className={"container"}>
        <TaskPage/>
    </main>
}

export default App;
