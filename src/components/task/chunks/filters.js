import React, { useState, useEffect} from 'react';
import {InputNumber, InputText} from "@libUI/components/input";

function Filters({
         filterHandler = () => {}
})
{
    const [filters, setFilters] = useState({});

    useEffect(() => {
        filterHandler(filters)
    }, [filters])

    return <div className={"filter_block"}>
        <InputNumber placeholder={"Номер заявки"} onChange={(value) => setFilters({...filters, task_id: value})}/>
        <InputText placeholder={"Наименование заявки"} onChange={(value) => setFilters({...filters, task_title: value})}/>
    </div>
}

export default Filters;