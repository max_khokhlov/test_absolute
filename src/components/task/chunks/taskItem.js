import React, {Component, useState, useEffect} from 'react';
import {formatPrice} from "@helpers"
import {InputCheckbox} from "@libUI"

function TaskItem(props){

    const {index, data} = props;

    const [checked, handleChecked] = useState(false);

    useEffect(() => {
        console.log(checked? "Выбран" : "Не выбран");
    }, [checked])

    return <div className={"task_item"} style={{zIndex: index}} >
        <div className="content">
            <div className="header_block">
                <div className="title">
                    {data.title}
                </div>
                <div className="checkbox">
                    <InputCheckbox onChange={handleChecked} />
                </div>
            </div>
            <div className="total_sum">
                {formatPrice(data.totalSum)} руб.
            </div>
            <div className="company_info">
                <div className="name">
                    {data.companyInfo.name}
                </div>
                <div className="inn">
                    ИНН: {data.companyInfo.inn}
                </div>
            </div>
            <div className="hidden_block">
                <div className="status_list">
                    {data.statusList.map(status => {
                        return <div className="status" key={status}>
                            {status}
                        </div>
                    })}
                </div>
                <div className="user">
                    {data.userInfo.name}
                </div>
                <div className="tag_list">
                    {data.tags.map(tag => <div className="tag_item" key={tag}>{tag}</div>)}
                </div>
            </div>
            <div className="footer_block">
                <div className="task_id">
                    {data.id}
                </div>
                <div className="date_create">
                   от {data.dateCreate}
                </div>
            </div>
        </div>
    </div>;
}

export default TaskItem;