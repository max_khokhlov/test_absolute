import {cloneDeep} from "lodash";

/**
 * Filters
 *
 * */


/**
 * Фильтрация
 * @param item
 * @param index
 * @param filters
 * @returns {boolean}
 */
const filterFuncForTask = ({item, index, filters}) => {

    const conformityFilterKey = {
        task_id: "id",
        task_title: "title"
    }

    for (let filterKey in filters){
        let valueItem = item[conformityFilterKey[filterKey]].toString().toLowerCase();
        let filterValue = filters[filterKey].toLowerCase();
        if (valueItem.indexOf(filterValue) > -1 && filterValue.length > 0){
            return true;
        }
    }

    return false;

}

/**
 * Проверка на пустоту фильтра
 * @param filters
 * @returns {boolean}
 */
const isNotEmptyFilters = (filters) => {

    if (!Object.values(filters)) return false;

    for(let value of Object.values(filters)){
        if (value.length > 0) return true;
    }
    return false;
}

/**
 *
 * @param filters
 * @param taskList
 * @returns {*}
 */
export const filterTasks = (filters, taskList) => {
    let filterTasks = cloneDeep(taskList);
    return isNotEmptyFilters(filters)? filterTasks.filter((item, index) => filterFuncForTask({item, index, filters})) : filterTasks;
}