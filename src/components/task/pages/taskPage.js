import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import * as actions from "@actions/taskAction"

//Chunks
import TaskItem from "../chunks/taskItem"
import Filters from "../chunks/filters"

//Helpers
import * as helpers from "../helpers";

const useFetchingGetTaskList = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(actions.getTaskList());
    }, [])
}

function TaskPage() {

    const {taskList, loading} = useSelector(state => state.tasks)
    const [filters, filterHandler] = useState({})
    const [filterTaskList, setFilterTaskList] = useState([])

    useFetchingGetTaskList();

    useEffect(() => {
        setFilterTaskList(helpers.filterTasks(filters, taskList));
    }, [taskList, filters])

    return (
        <section className={"task_block"}>
            <Filters filterHandler={filterHandler}/>
            <div className="task_list">
                {
                    loading?
                        <div>Загрузка...</div>
                        :
                        filterTaskList.map((task, index) => <TaskItem data={task} index={filterTaskList.length - index} key={index.toString()} />)
                }
            </div>
        </section>
    );

}


export default TaskPage