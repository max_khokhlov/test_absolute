import {store} from "./../index";
import * as common from "@constants/common"
import {cloneDeep} from "lodash";

export const errorMessage = (code, message) => {
    store.dispatch({
        type: common.ERROR_MESSAGE,
        code: code,
        message: message
    })
}

export const formatPrice = (price) => {
    if (isNaN(parseInt(price))) return 0;
    return parseInt(price).toLocaleString('ru-RU')
}

