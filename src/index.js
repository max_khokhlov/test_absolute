import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App"
import { Provider } from 'react-redux';

//style
import "./_build/scss/main.scss"
import "@libUI/styles/main_libui.scss"

import configureStore from './store';

export const store = configureStore({});

ReactDOM.render(
  <Provider store={store}>
      <App />
  </Provider>,
  document.querySelector("#root")
);
