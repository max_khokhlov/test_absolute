import React, {Component, useState, useEffect} from 'react';

function InputCheckbox (props) {

   const [value, setValue] = useState(false)

    useEffect(() => {
        if (props.onChange){
            props.onChange(value)
        }
    }, [value])

   return (
       <div className={"input_block"}>
           <div className="checkbox">
               <input type="checkbox" checked={value} onChange={() => setValue(!value)}/>
               <label></label>
           </div>
       </div>
   );
}

export default InputCheckbox;