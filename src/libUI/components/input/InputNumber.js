import React, {Component, useState, useEffect} from 'react';

function InputNumber(props){

    const [value, setValue] = useState("")

    useEffect(() => {
        if (props.onChange){
            props.onChange(value)
        }
    }, [value])

    return (
        <div className="input_block">
            <input type="number" placeholder={props.placeholder || "Введите текст"} value={value} onChange={e => setValue(e.target.value)} min={props.min || 0} max={props.max || 99999999999999} step={props.step || 1}/>
            {props.icon || <i className="icon-search"></i>}
        </div>
    );
}

export default InputNumber;