import React, {Component, useEffect, useState} from 'react';

function InputText(props){

    const [value, setValue] = useState("")

    useEffect(() => {
        if (props.onChange){
            props.onChange(value)
        }
    }, [value])

    return (
        <div className="input_block">
            <input type="text" placeholder={props.placeholder || "Введите текст"}  value={value} onChange={e => setValue(e.target.value)}/>
            {props.icon || <i className="icon-search"></i>}
        </div>
    );
}

export default InputText;