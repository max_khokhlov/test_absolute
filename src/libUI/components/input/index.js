import InputText from "./InputText"
import InputNumber from "./InputNumber"
import InputCheckbox from "./InputCheckbox"


export {
    InputText,
    InputNumber,
    InputCheckbox
}