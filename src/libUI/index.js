//Components
import {InputText, InputNumber, InputCheckbox} from "./components/input"

export {
    InputText,
    InputNumber,
    InputCheckbox
}