import initialState from "./initialState"
import * as c from "@constants/common"

export default function(state = initialState, action) {
    switch (action.type) {
        case c.ERROR_MESSAGE:
            return {
                ...state,
                error: {
                    code: action.code,
                    message: action.message
                }
            };
        default:
            return state
    }
}