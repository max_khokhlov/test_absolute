import { combineReducers } from 'redux'

import tasks from "./tasks"
import common from "./common"

export default combineReducers({
    common,
    tasks
})


