import initialState from "./initialState"
import * as c from "@constants/tasks"

export default function(state = initialState, action) {
    switch (action.type) {
        case c.LOAD_TASK_LIST:
            return {
                ...state,
                loading: action.loading
            };
        case c.SET_TASK_LIST:
            return {
                ...state,
                taskList: action.taskList
            };
        default:
            return state
    }
}