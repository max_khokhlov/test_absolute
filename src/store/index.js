import { applyMiddleware, compose, createStore } from 'redux';
import rootReducer from '@reducers';
import { createLogger } from 'redux-logger';
import ReduxThunk from 'redux-thunk';

export default function configureStore(initialState) {
    const logger = createLogger();
    const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const middleware = process.env.NODE_ENV === "development" ? applyMiddleware(ReduxThunk, logger) : applyMiddleware(ReduxThunk);
    return createStore(rootReducer, initialState, composeEnhancers(middleware));
}