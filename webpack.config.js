const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')
const CopyWebpackPlugin = require('copy-webpack-plugin');

const devMode = __DEV__ = process.env.NODE_ENV !== "production";

module.exports = {
  entry: [
    '@babel/polyfill',
    "./src/index.js"
  ],
  mode: "development",
  output: {
    filename: "./js/main.js",
    chunkFilename: "[name].bundle.js"
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9000,
    watchContentBase: true,
    progress: true
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader"
        }
      },
      { test: /\.(le|sa|sc|c)ss$/, use: [
          devMode ? { loader: 'style-loader'}  : {loader: MiniCssExtractPlugin.loader},
          {loader: 'css-loader'},
          {loader: 'postcss-loader',
            options: {
              plugins: [
                autoprefixer({
                  browsers:['ie >= 8', 'last 100 version']
                })
              ],
              sourceMap: true
            }},
          {loader: 'sass-loader'}
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: './fonts'
          }
        }]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"]
      }
    ]
  },
  resolve: {
    alias: {
      "@libUI": path.resolve(__dirname, 'src/libUI/'),
      "@components": path.resolve(__dirname, 'src/components/'),
      "@reducers": path.resolve(__dirname, 'src/reducers/'),
      "@actions": path.resolve(__dirname, 'src/actions/'),
      "@constants": path.resolve(__dirname, 'src/constants/'),
      "@helpers": path.resolve(__dirname, 'src/helpers'),
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new MiniCssExtractPlugin({
      filename: devMode ? './css/[name].css' : './css/[name].css',
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: './src/_build/fonts',
          to: './fonts'
        }
      ]
    }),
  ]
};
